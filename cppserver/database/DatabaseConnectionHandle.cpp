#include "DatabaseConnectionHandle.h"

DatabaseConnectionHandle::DatabaseConnectionHandle(DatabaseConnectionPoolPtr dcp)
  :m_dcp(dcp),
   m_id(m_dcp->get_connection()),
   m_db(m_dcp->GetDatabase(m_id))
{}

DatabaseConnectionHandle::~DatabaseConnectionHandle()
{
  m_dcp->release_connection(m_id);
}

MYSQL* DatabaseConnectionHandle::db() noexcept
{
  return m_db;
}
