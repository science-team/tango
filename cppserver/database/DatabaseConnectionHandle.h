#ifndef DATABASECONNECTIONMANAGER_H
#define DATABASECONNECTIONMANAGER_H

#include "DatabaseConnectionPool.h"
#include "common.h"

/// @brief RAII class for database connections
class DatabaseConnectionHandle
{
public:
  DatabaseConnectionHandle(DatabaseConnectionPoolPtr dcp);
  ~DatabaseConnectionHandle();

  DatabaseConnectionHandle(const DatabaseConnectionHandle&) = delete;
  DatabaseConnectionHandle& operator=(const DatabaseConnectionHandle&) = delete;
  DatabaseConnectionHandle(DatabaseConnectionHandle&&) = delete;
  DatabaseConnectionHandle& operator=(DatabaseConnectionHandle&&) = delete;

  /// Allow access to the database class itself
  /// This pointer must *never* stored in a variable as it's lifetime is tied to the lifetime
  /// of this class.
  MYSQL* db() noexcept;

private:
  DatabaseConnectionPoolPtr m_dcp;
  int m_id;
  MYSQL* m_db = nullptr;
};

#endif // DATABASECONNECTIONMANAGER_H
