#ifndef TANGO_DB_COMMON_H
#define TANGO_DB_COMMON_H

#include <cmake_config.h>

#include <tango/tango.h>
#include <mysql.h>
#include "update_starter.h"

#if !defined(LIBMARIADB) && !defined(MARIADB_BASE_VERSION)
#if MYSQL_VERSION_ID >= 80001 && MYSQL_VERSION_ID != 80002
typedef bool my_bool;
#endif
#endif

#ifdef _TG_WINDOWS_
#include <ws2tcpip.h>
#include <windows.h>
static inline void sleep(DWORD seconds)
{
    Sleep(seconds * 1000);
}
#else
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#endif

#define DB_SQLError              "DB_SQLError"
#define DB_IncorrectArguments    "DB_IncorrectArguments"
#define DB_IncorrectDeviceName   "DB_IncorrectDeviceName"
#define DB_IncorrectServerName   "DB_IncorrectServerName"
#define DB_DeviceNotDefined      "DB_DeviceNotDefined"
#define DB_AliasNotDefined       "DB_AliasNotDefined"
#define DB_NoFreeMySQLConnection "DB_NoFreeMySQLConnection"
#define DB_MySQLLibNotThreadSafe "DB_MySQLLibNotThreadSafe"
#define DB_ParseStringError      "DB_ParseStringError"

#define STARTER_DEVNAME_DOMAIN "tango"
#define STARTER_DEVNAME_FAMILY "/admin/"

#define	DEFAULT_CONN_POOL_SIZE		20

//	Define time measuremnt type (depends on OS)
#ifndef WIN32

#	define	TimeVal	struct timeval
#	define	GetTime(t)	gettimeofday(&t, NULL);
#	define	Elapsed(before, after)	\
		1000.0*(after.tv_sec-before.tv_sec) + \
		((double)after.tv_usec-before.tv_usec) / 1000

#else

static LARGE_INTEGER	cpu_freq;
#	define	TimeVal	LARGE_INTEGER
#	define	GetTime(t)	w_gettimeofday(&t);
#	define	Elapsed(before, after)		\
		(cpu_freq.QuadPart==0) ? 0.0 :	\
			(double) (after.QuadPart - before.QuadPart)/cpu_freq.QuadPart * 1000;

#endif	/*	WIN32		*/

#endif //TANGO_DB_COMMON_H
