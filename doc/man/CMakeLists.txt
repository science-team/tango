project(man)

include(GNUInstallDirs)

set(MANPAGES astor.1
		         atkmoni.1
		         atkpanel.1
		         atktuning.1
		         jdraw.1
		         jive.1
		         logviewer.1
		         pogo.1
		         synopticappli.1)

foreach(input ${MANPAGES})
  install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/${input}
          DESTINATION ${CMAKE_INSTALL_MANDIR}/man1)
endforeach()
